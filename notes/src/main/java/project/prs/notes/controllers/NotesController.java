package project.prs.notes.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import project.prs.notes.dao.NotesDAO;
import project.prs.notes.exceptions.NoteNotFoundException;
import project.prs.notes.resources.Note;

import javax.validation.Valid;
import java.net.URI;

@RestController
public class NotesController
{
    NotesDAO notesDAO = NotesDAO.getInstance();

    @PutMapping(path = "/notes")
    public ResponseEntity createNote(@Valid @RequestBody Note note)
    {
        notesDAO.insertNote(note);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(note.getId())
                .toUri();
        return ResponseEntity.created(location).body("New note Created");
    }

    @GetMapping(path = "/notes/{id}")
    public ResponseEntity getNoteById(@PathVariable("id") Integer id)
    {
        Note note = notesDAO.getNoteById(id);

        if (note != null)
        {
            ObjectMapper mapper = new ObjectMapper();
            String response = "";
            try
            {
                response = mapper.writeValueAsString(note);
            }
            catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
            return ResponseEntity.ok().body(response);
        }
        else
        {
            throw new NoteNotFoundException("id-"+id);
        }
    }

    @GetMapping(path = "/notes", params = {"!title"})
    public ResponseEntity getAllNotes()
    {
        ObjectMapper mapper = new ObjectMapper();
        String response="";

        try
        {
            response = mapper.writeValueAsString(notesDAO.getAllNotes());
        }
        catch (JsonProcessingException e)
        {
            e.printStackTrace();
        }

        return ResponseEntity.ok().body(response);
    }

    @GetMapping(path = "/notes", params = {"title"})
    public ResponseEntity getNotesByTitle(@RequestParam String title)
    {
        Note note = notesDAO.getNoteByTtile(title);

        if (note != null)
        {
            ObjectMapper mapper = new ObjectMapper();
            String response = "";
            try
            {
                response = mapper.writeValueAsString(note);
            }
            catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
            return ResponseEntity.ok().body(response);
        }
        else
        {
            throw new NoteNotFoundException("title: "+ title);
        }
    }

    @DeleteMapping(path = "/notes/{id}")
    public ResponseEntity deleteNoteById(@PathVariable Integer id)
    {
        boolean status = notesDAO.deleteNoteById(id);

        if(status)
        return ResponseEntity.ok().body("Note deleted");

        throw new NoteNotFoundException("Note was not found.");
    }
}
