package project.prs.notes.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import project.prs.notes.resources.Note;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class NotesDAO
{
    private SessionFactory factory;
    private AtomicInteger noteId;

    private static volatile NotesDAO notesDAO = null;

    private NotesDAO()
    {
        noteId = new AtomicInteger(0);
    }

    public static NotesDAO getInstance()
    {
        if(notesDAO == null)
        {
            synchronized (NotesDAO.class)
            {
                if(notesDAO == null)
                {
                    notesDAO = new NotesDAO();
                }
            }
        }

        return notesDAO;
    }

    public void insertNote(Note note)
    {

        factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Note.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        note.setId(noteId.incrementAndGet());
        note.setCreatedOn(new Date());

        //Use the session to save the java object.
        try
        {
            //Begin transaction.
            session.beginTransaction();

            //Save the note.
            session.save(note);
            System.out.println("Saved the note");

            //Commit the transaction
            session.getTransaction().commit();
        }
        finally
        {
            factory.close();
        }

    }

    public Note getNoteById(Integer id)
    {
        factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Note.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        Note note = null;
        try
        {
            session.beginTransaction();

            note = session.get(Note.class, id);
            System.out.println("Got the student");

            session.getTransaction().commit();
        }
        finally
        {
            factory.close();
        }

        return note;
    }

    public List<Note> getAllNotes()
    {
        factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Note.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        List<Note> listOfNotes = null;
        try
        {
            session.beginTransaction();

            listOfNotes = session
                    .createQuery("from Note")
                    .getResultList();

            session.getTransaction().commit();
        }
        finally
        {
            factory.close();
        }

        return listOfNotes;
    }

    public Note getNoteByTtile(String title)
    {
        factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Note.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        Note note = null;

        try
        {
            session.beginTransaction();

            List<Note> noteList = session
                    .createQuery("from Note n where " + "n.title LIKE '%" + title + "'")
                    .getResultList();

            session.getTransaction().commit();

            if((noteList != null) && (noteList.size() > 0))
            {
                note = noteList.get(0);
            }
        }
        finally
        {
            factory.close();
        }

        return note;
    }

    public boolean deleteNoteById(Integer id)
    {
        factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Note.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        Note note = null;

        int count = 0;
        try
        {
            session.beginTransaction();

            count = session
                    .createQuery("delete from Note where id=" + id)
                    .executeUpdate();

            session.getTransaction().commit();

        }
        finally
        {
            factory.close();
        }

        if(count > 0)
        {
            return true;
        }
        return false;
    }
}
