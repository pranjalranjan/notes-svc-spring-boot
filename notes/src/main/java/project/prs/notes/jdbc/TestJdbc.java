package project.prs.notes.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestJdbc
{
    public static void main(String[] args)
    {
        String jdbcUrl = "jdbc:postgresql://localhost:5432/notes_db";
        String username = "prs";
        String password = "prs123";

        Connection connection = null;
        try
        {
            connection = DriverManager.getConnection(jdbcUrl, username, password);
            System.out.println("Connection Successful!");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if(connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
