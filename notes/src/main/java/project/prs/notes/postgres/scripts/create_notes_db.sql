CREATE TABLE notes(
	note_id serial PRIMARY KEY,
	title VARCHAR (50) NOT NULL,
	note VARCHAR (355) NOT NULL,
	created_on TIMESTAMP  NOT NULL
);